<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SecondaryController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('Element/Header/Header');
		$this->load->view('Secondary/SecondaryView');
		$this->load->view('Element/Footer/Footer');
	}
}