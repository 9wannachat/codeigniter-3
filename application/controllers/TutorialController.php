<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TutorialController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		// initial model
		$this->load->model('TutorialModel');
	}

	public function index()
	{
		//Receive Data from Model
		$data['query'] = $this->TutorialModel->GetData();
		
		// load and render Views
		$this->load->view('Element/Header/Header');
		$this->load->view('Tutorial/TutorialView', $data);
		$this->load->view('Element/Footer/Footer');
	}
}