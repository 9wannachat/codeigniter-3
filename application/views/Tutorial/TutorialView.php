<div class="container">
    <div class="mt-5"></div>
    <h1 class="text-center">Hello World Tutorial</h1>
    <hr>
    <div class="text-center mb-5 display-3" id="test-div"></div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($query as $x) {?>
                <tr>
                    <td><?php echo $x->testid; ?></td>
                    <td><?php echo $x->testname; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script>
    $('#test-div').html('Hello JQuery')
</script>