<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tutorial</title>
    <!-- Css Lib -->
    <link rel="stylesheet" href="<?= site_url('assets/lib/bootstrap/dist/css/bootstrap.min.css');?>">

    <!-- JS Lib -->
    <script src="<?= site_url('assets/js/site.js')?>"></script>
    <script src="<?= site_url('assets/lib/jquery/dist/jquery.min.js');?>"></script>
</head>
<body>